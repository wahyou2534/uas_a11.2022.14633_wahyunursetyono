<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <!-- Jquery Scrooll CSS -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">


        <!-- Tittle -->
        <title>Sido-Waras-Jamu-Beras-Kencur</title>
    </head>
    <body style="background-image: url(assets/beras-kencur-background.jpeg);">
        <!-- ===== Header ===== -->
        <?php include 'include/header.php'; ?>
        <!-- ===== End Header ===== -->

        <!-- =====  Beras Kencur ===== -->
        <?php include 'include/beras-kencur-content.php'; ?>
        <!-- ===== End Beras Kencur ===== -->

        <!-- ===== Footer ===== -->
        <?php include 'include/footer.php'; ?>
        <!-- ===== End Footer ===== -->

        <!-- Script JS Scroll Animation -->
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
          AOS.init();
        </script>

        <!-- Script Bootstrap -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </body>
</html>