<!-- =====  Brotowali ===== -->
<div class="container">
    <div class="caption" id="brotowali-caption">
        <h1 data-aos="fade-down" data-aos-duration="2000">Jamu Brotowali</h1>
        <br>
        <img src="assets/brotowali.jpg" alt="brotowali" data-aos="flip-right" data-aos-duration="2000">
        <br>
        <ol type="A">
            <h4 data-aos="zoom-out-right" data-aos-duration="2000"><li>Pengertian Jamu Brotowali</li></h4>
            <p data-aos="zoom-out-left" data-aos-duration="2000">
                Brotowali merupakan tanaman obat alami yang seluruh bagiannya dapat bermanfaat untuk menjaga kesehatan tubuh. Akar  brotowali bisa dijadikan sebagai jamu yang memiliki banyak manfaat. Kamu bisa memperoleh jamu brotowali dengan merebus batangnya. Meskipun banyak orang enggan mengonsumsi brotowali karena rasa pahitnya sangat menyengat.
            </p>
            <br><br><br>
            <h4 data-aos="zoom-out-right" data-aos-duration="2000"><li>Manfaat Jamu Brotowali</li></h4>
            <br><br>
            <ol type="a" data-aos="zoom-out-left" data-aos-duration="2000">
                Di bawah ini beragam manfaat Jamu Brotowali bagi kesehatan tubuh :
                <li>Menjaga Kesehatan Jantung</li>
                Tanaman Brotowali mengandung senyawa antioksidan yang dapat menurunkan kadar kolesterol LDL dan trigliserida di dalam darah, sehingga manfaat jamu Brotowali ini dapat mencegah penyumbatan yang mengganggu aliran darah, agar aliran darah lancar dan terhindar dari stroke serta serangan jantung.
                <br><br>
                <li>Menurunkan Tekanan Darah</li>
                Jamu brotowali memiliki manfaat dalam memperlancar aliran darah, dan dapat menurunkan tekanan darah yang disebabkan oleh kolesterol, lemak, dan zat lainnya yang terdapat pada pembuluh arteri.
                <br><br>
                <li>Mengatasi Demam</li>
                Manfaat jamu brotowali yang juga cukup dikenal masyarakat yaitu manfaat jamu Brotowali dalam membantu penurunan demam pada tubuh. Tanaman Brotowali sering dijadikan sebagai obat penurun demam karena manfaat jamu Brotowali salah satunya memiliki efek antipiretik serta analgesik atau dapat meredakan nyeri. Untuk mengatasi kondisi demam, dibutuhkan sebuah obat berjenis antipiretik yang berfungsi agar dapat membantu menurunkan suhu tubuh kembali menjadi normal.
                <br><br>
            </ol>
            <br>
            <h4 data-aos="zoom-out-right" data-aos-duration="2000"><li>Cara Membuat Jamu Brotowali</li></h4>
            <br><br>                    
            <ul style="list-style-type:circle;" data-aos="zoom-out-left" data-aos-duration="2000">
                Berikut ini resep jamu brotowali sesuai dengan manfaat yang dapat dicoba :
                <li>Bahan :</li>
                    <ul style="list-style-type: square;">
                        <li>2 ruas temu hitam (temu ireng)</li>
                        <li>2 ruas kunyit</li>
                        <li>1 ruas temulawak</li>
                        <li>2 ruas brotowali</li>
                        <li>4 lembar daun sirih</li>
                        <li>500 ml air</li>
                        <li>2 sdm gula aren sisir</li>
                        <li>1 sdm asam jawa</li>
                        <li>50 ml air panas</li>
                    </ul>
                <br><br>
                <li>Cara Membuat :</li>
                    <ol type="1">
                        <li>Kupas semua bahan dan cuci bersih.</li>
                        <li>Tumbuk temu hitam, kunyit, temulawak, dan brotowali hingga halus.⁣⁣ Rebus dengan wadah tembikar atau panci enamel bersama air dan daun sirih hingga mendidih.</li>
                        <li>Tambahkan gula aren sisir dan air asam jawa, didihkan sebentar sambil diaduk rata. Saring, dinginkan.</li>
                        <li>Dianjurkan untuk diminum 2 hari sekali saja sebanyak 1 gelas.⁣⁣</li>
                    </ol>
            </ul>
        </ol>
    </div>
</div>
<!-- ===== End Brotowali ===== -->