<!-- =====  Kunci Sirih ===== -->
<div class="container">
    <div class="caption" id="kunci-sirih-caption">
        <h1 data-aos="fade-down" data-aos-duration="2000">Jamu Kunci Sirih</h1>
        <br>
        <img src="assets/kunci-sirih.jpg" alt="kunci-sirih" data-aos="flip-down" data-aos-duration="2000">
        <br>
        <ol type="A">
            <h4 data-aos="zoom-in-down" data-aos-duration="2000"><li>Pengertian Jamu Kunci Sirih</li></h4>
            <p data-aos="zoom-in-up" data-aos-duration="2000">
                Jamu kunci sirih merupakan perpaduan dari daun sirih dan temu kunci. Seperti yang sudah diketahui oleh banyak orang, daun sirih memiliki manfaat yang baik untuk wanita, yaitu mengatasi masalah keputihan, menghilangkan bau badan tidak sedap, dan mengatasi jerawat. Jamu kunci sirih juga dapat merapatkan organ kewanitaan, memperkuat email gigi dan mencegah peradangan. Selain terdiri dari daun sirih dan kunci temu, minuman ini juga dicampur dengan bahan-bahan seperti kunyit, kencur, jahe, kayu manis, kapulaga, asam jawa, serai, perasan jeruk nipis, garam, dan gula merah.
            </p>
            <br><br><br>
            <h4 data-aos="zoom-in-down" data-aos-duration="2000"><li>Manfaat Jamu Beras Kencur</li></h4>
            <p data-aos="zoom-in-up" data-aos-duration="2000">
                Salah satu jamu yang banyak dicari dahulu adalah jamu kunci sirih. Meskipun tidak sepopuler jamu beras kencur dan kunyit asem, jamu yang satu ini memiliki kegunaan yang para wanita ingin memilikinya. Dikemukakan dalam manfaatdankhasiat.com, jamu yang satu ini sangat berguna bagi para wanita yang baru saja melahirkan. Mereka bisa mendapatkan bentuk tubuh indah mereka kembali jika ibu-ibu yang baru melahirkan tersebut meminum jamu yang satu ini dengan teratur.
                <br><br>
                Selain bisa mengembalikan kelangsingan tubuh Anda setelah melahirkan, masih banyak lagi khasiat dari jamu kunci sirih ini. Dilansir dalam ipbkpm.wordpress.com, khasiat-khasiat tersebut meliputi meredakan keputihan yang sering diderita oleh wanita, merapatkan daerah kewanitaan Anda, memperkuat gigi Anda dan menghilangkan bau badan yang muncul karena keringat berlebih.
                <br><br>
                Khasiat yang begitu besar ini tentu akan sangat sayang untuk dilewatkan lho, Ladies. Jadi, jika masih memungkinkan, carilah para penjual jamu yang bisa menyediakan Anda dengan jamu kunci sirih ini. Anda mungkin masih bisa menemukan mereka di pasar-pasar tradisional.
                <br><br><br>
            </p>
            <br>
            <h4 data-aos="zoom-in-down" data-aos-duration="2000"><li>Cara Membuat Jamu Kunci Sirih</li></h4>
            <ul style="list-style-type:circle;" data-aos="zoom-in-up" data-aos-duration="2000">
            Berikut ini resep jamu brotowali sesuai dengan manfaat yang dapat dicoba :
            <br><br>                    
                <li>Bahan :</li>
                    <ul style="list-style-type: square;">
                        <li>1 liter air</li>
                        <li>1 kunyit</li>
                        <li>2 jari jahe</li>
                        <li>1 buah jeruk nipis</li>
                        <li>25 gram asam jawa</li>
                        <li>1 ruas kulit kayu manis</li>
                        <li>2 batang serai</li>
                        <li>5 ruas kencur</li>
                        <li>5 biji kapulaga</li>
                        <li>4 ruas temu kunci</li>
                        <li>Segenggam daun luntas</li>
                        <li>Segenggam daun sirih</li>
                        <li>1/4 ons gula merah</li>
                    </ul>
                <br><br>
                <li>Cara Membuat :</li>
                    <ol type="1">
                        <li>Siapkan blender, masukkan seluruh bahan kecuali serai, asam jawa, kayu manis, gula merah dan jeruk nipis. Blender hingga halus lalu saring.</li>
                        <li>Siapkan panci, tuangkan air, nyalakan api. Masukkan bahan-bahan yang sudah diblender tadi, lalu rebus.</li>
                        <li>Masukkan gula merah, asam jawa, serai, dan kayu manis, lalu rebus.</li>
                        <li>Tambahkan air jeruk nipis. Jika sudah mendidih, maka kecilkan api, diamkan selama 15 menit, lalu angkat dan dinginkan.</li>
                        <li>Jamu Kunci Suruh siap disajikan.</li>
                    </ol>
            </ul>
        </ol>
    </div>
</div>
<!-- ===== End Kunci Sirih ===== -->