<!-- ===== Feedback ===== -->
<div class="container">
    <div class="caption" id="feedback-caption" data-aos="zoom-in" data-aos-duration="2000">
        <h1>Feedback</h1>
        <form action="your-feedback.php" method="POST">
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Enter Your Name" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="Enter Your Email" required>
            </div>
            <div class="mb-3">
                <label for="subject" class="form-label">Subject</label>
                <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter Your Subject" required>
            </div>
            <div class="mb-3">
                <label for="message" class="form-label">Message</label>
                <textarea type="text" name="message" class="form-control" id="message" placeholder="Enter Your Message" required></textarea>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<!-- ===== End Feedback ===== -->