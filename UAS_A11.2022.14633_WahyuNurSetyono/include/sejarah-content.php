<!-- =====  Sejarah ===== -->
<div class="container">
    <div class="caption" id="sejarah-caption">
        <h1 data-aos="fade-down" data-aos-duration="2000">Jamu Tradisional</h1><br>
        <img src="assets/jamu-tradisional.jpg" alt="jamu-tradisional" data-aos="zoom-in" data-aos-duration="2000">
        <br>
        <ol type="A">
            <h4 data-aos="fade-right" data-aos-duration="2000"><li>Pengertian Jamu Tradisional</li></h4>
                <p data-aos="fade-left" data-aos-duration="2000">
                    Jamu adalah sebuah obat tradisional khas Indonesia. Jamu terbuat dari bahan alami tumbuh-tumbuhan seperti rimpang (akar-akaran), dedaunan, kulit batang, dan buah. Ada juga yang terbuat dari tubuh hewan. Sesekali juga kuning telur ayam kampung ditambahkan dalam membuat jamu tersebut.<br><br>
                    Di beberapa kota besar di Indonesia banyak dijumpai profesi penjual jamu gendong keliling. Selain itu, juga terdapat Perusahaan pabrik jamju besar seperti Jamu Air Mancur, Nyonya Meneer , Jamu Sabdo Palon, Djamu Djago, dan yang lainnya. Pabrik tersebut memproduksi jamu dalam bentuk sachet yang sangat praktis. Pada perkembangan selanjutnya, mungkin jamu juga akan dijual dalam bentuk tablet, kaplet dan kapsul.
                </p>
            <br><br><br>
            <h4 data-aos="fade-right" data-aos-duration="2000"><li>Sejarah Jamu Tradisional</li></h4>
                <p data-aos="fade-left" data-aos-duration="2000">
                    Umumnya jamu digunakan oleh masyarakat Indonesia sebagai minuman obat alami untuk menyembuhkan berbagai penyakit dan menjaga kesehatan. Tradisi minum jamu ini diperkirakan sudah ada sejak 1300 M dan merupakan minuman bersejarah. <br><br>
                    Perkembangan jamu mengalami pasang surut sesuai zamannya. Secara garis besar terbagi dari zaman pra-sejarah saat pengolahan hasil hutan marak berkembang, zaman penjajahan jepang, zaman awal kemerdekaan Indonesia, hingga saat ini. Masyarakat Indonesia sejak zaman Kerajaan Mataram hingga kini masih menggunakan Jamu. Minuman khas Indonesia ini telah menjadi kebanggaan tersendiri seperti halnya dengan Ayurveda dari India dan Zhongyi dari Cina. Sejak saat itu, perempuan lebih berperan dalam memproduksi jamu, sedangkan pria berperan mencari tumbuhan herbal alami. Fakta itu diperkuat dengan adanya temuan artefak Cobek dan Ulekan –alat tumbuk untuk membuat jamu. Artefak itu bisa dilihat di situs arkeologi Liyangan yang berlokasi di lereng Gunung Sindoro, Jawa Tengah. <br><br>
                    Perlu diketahui, Jamu dipercaya berasal dari dua kata Jawa Kuno, Djampi yang bermakna penyembuhan dan Oesodo yang bermakna kesehatan. Istilah Jamu diperkenalkan ke publik lewat orang-orang yang dipercaya punya ilmu pengobatan tradisonal. Mesti tak bersetifikat, khasiat Jamu telah teruji oleh waktu secara turun-temurun digunakan sebagai obat tradisional. Sehingga hingga saat ini, minuman berkhasiat khas Indonesia ini selalu terjaga keberlangsungannya. Warisan nenek moyang yang tetap dijaga sampai kapan pun. 
                </p>
            <br><br><br>
            <h4 data-aos="fade-right" data-aos-duration="2000"><li>Khasiat Jamu Tradisional</li></h4>
                <p data-aos="fade-left" data-aos-duration="2000">
                    Jamu di Indonesia biasanya digunakan sebagai obat herbal (hasil maracik bahan-bahan alami dari alam) yang memiliki khasiat untuk kesehatan. Jamu tidak hanya berfungsi sebagai obat, tetapi juga untuk menjaga kebugaran tubuh, mencegah terkena penyakit, dan menyembuhkan penyakit. Jamu juga memiliki khasiat membantu meningkatkan nafsu makan bagi anak-anak. Jamu di Indonesia bukan sekadar ramuan tradisional yang berkhasiat. Akan tetapi, mempunyai upaya untuk tetap menjaga kelestarian alam. Jamu yang memanfaatkan bahan-bahan alam akan mendorong manusia untuk menanam kembali tanaman-tanamantumbuh-tumbuhan yang digunakan sebagai bahan membuat jamu.
                </p>
                <br><br><br>
            <h4 data-aos="fade-right" data-aos-duration="2000"><li>Contoh Jamu Tradisional</li></h4>
                <ul data-aos="fade-left" data-aos-duration="2000">
                    <li>Beras Kencur,</li>
                        Jamu campuran dari ekstrak jahe, beras, kencur, dan asam jawa. Rasanya manis dan segar. Mengandung antioksidan tinggi yang dapat melindungi sel tubuh dari kerusakan akibat radikal bebas. 
                        <br><br>
                    <li>Kunyit Asam,</li>
                        Jamu terbuat dari kunyit, temulawak, asam jawa, dan gula merah. Warna kuning pada kunyit asam berasal dari senyawa kurkumin. Senyawa ini mengandung antioksidan yang bersifat antiinflamasi dan antikanker.
                        <br><br>
                    <li>Brotowali,</li>
                        Jamu ini juga dikenal dengan sebutan ‘pahitan’. Bahan utamanya dari daun sambiloto. Jamu ini bermanfaat untuk meredakan gatal-gatal, mengatasi pegal, mencegah diabetes, serta menambah nafsu makan.
                        <br><br>
                    <li>Kunci Sirih</li>
                        Jamu yang dibuat dari rimpang kunci dan daun sirih. Kunci sirih bermanfaat untuk mengatasi keputihan, merapatkan bagian kewanitaan, memperkuat gigi, dan menghilangkan bau badan.
                </ul>
        </ol>
    </div>
</div>
<!-- ===== End Sejarah ===== -->