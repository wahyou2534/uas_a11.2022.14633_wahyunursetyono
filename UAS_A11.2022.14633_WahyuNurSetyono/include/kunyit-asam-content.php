<!-- =====  Kunyit Asam ===== -->
<div class="container">
    <div class="caption" id="kunyit-asam-caption">
        <h1 data-aos="fade-down" data-aos-duration="2000">Jamu Kunyit Asam</h1>
        <br>
        <img src="assets/kunyit-asam.jpg" alt="kunyit-asam" data-aos="flip-left" data-aos-duration="2000">
        <br>
        <ol type="A">
            <h4 data-aos="zoom-in-left"><li>Pengertian Jamu Kunyit Asam</li></h4>
            <p data-aos="zoom-in-right" data-aos-duration="2000">
                Kunyit asam adalah minuman tradisional atau jamu yang diracik dari dua jenis rempah yang berbeda, yaitu kunyit dan asam Jawa. Di Indonesia, kedua rempah ini tidak hanya bisa digunakan sebagai bumbu masakan, tetapi juga diolah menjadi minuman yang dikenal berkhasiat bagi kesehatan. Kunyit  biasa digunakan sebagai bahan masakan, seperti kari dan soto. Namun siapa sangka, selain bisa diolah menjadi masakan, rempah berwarna kuning oranye ini ternyata juga bisa digunakan sebagai obat.
            </p>
            <br><br><br>
            <h4 data-aos="zoom-in-left" data-aos-duration="2000"><li>Manfaat Jamu Kunyit Asam</li></h4>
            <br><br>
            <ol type="a" data-aos="zoom-in-right" data-aos-duration="2000">
                Berikut ini adalah beberapa manfaat kunyit untuk kesehatan :
                <li>Melawan Infeksi</li>
                Kunyit mengandung zat yang dapat melawan jamur, parasit, virus, dan bakteri. Dengan kata lain, kunyit dapat Anda manfaatkan untuk mencegah dan melawan terjadinya infeksi. Untuk mengobati luka atau luka bakar, kunyit harus dihaluskan dan dijadikan pasta atau bubur yang kemudian dioleskan pada luka, bukan berupa minuman kunyit asam.
                <br><br><br>
                <li>Melindungi Usus dan Lambung</li>
                Kunyit juga dipercaya baik untuk memelihara kesehatan organ pencernaan. Kunyit yang mengandung zat antiradang dan antioksidan ini telah terbukti dapat meredakan gangguan pencernaan dan melindungi hati. Lebih jauh lagi, rempah ini dapat membantu pengobatan dan mencegah timbulnya tukak lambung.
                <br><br>
                <li>Mengurangi Kolesterol</li>
                Beberapa studi menunjukkan bahwa orang yang rutin mengonsumsi kunyit atau suplemen kurkumin mengalami penurunan kadar kolesterol jahat (LDL). Hal ini baik untuk mencegah penyakit jantung dan membantu mengurangi kadar kolesterol pada orang yang memiliki kolesterol tinggi.
                <br><br>
            </ol>
            <h4 data-aos="zoom-in-left" data-aos-duration="2000"><li>Cara Membuat Jamu Kunyit Asam</li></h4>
            <ul style="list-style-type:circle;" data-aos="zoom-in-right" data-aos-duration="2000">
                Cara membuat jamu kunyit asam tidak sulit, bisa dibuat di rumah dan dikonsumsi sekeluarga. Berikut resepnya :
                <br><br>
                <li>Bahan :</li>
                    <ul style="list-style-type: square;">
                        <li>200 gram kunyit</li>
                        <li>250 gram gula</li>
                        <li>100 gram asam jawa</li>
                        <li>1.500 ml air</li>
                        <li>1/2 sdt garam</li>
                    </ul>
                <br><br>
                <li>Cara Membuat :</li>
                    <ol type="1">
                        <li>Bakar kunyit kemudian parut halus.</li>
                        <li>Rebus kunyit dan semua semua bahan lainnya sampai mendidih. Saring dan dinginkan. Simpan di botol kaca jika ingin disimpan di kulkas.</li>
                        <li>Kunyit asam bisa dinikmati panas atau dingin.</li>
                    </ol>
            </ul>
        </ol>
    </div>
</div>
<!-- ===== Kunyit Asam ===== -->