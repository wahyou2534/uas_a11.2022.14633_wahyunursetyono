<!-- ===== Footer ===== -->
<footer class="footer bg-light fixed-bottom mt-10" id="footer">
    <div class="container">
        <div class="copyright">
            &copy; Copyright <span>Sido-Waras</span> 2023
        </div>
</footer>
<!-- ===== End Footer ===== -->