<!-- ===== Home ===== -->
<div class="container">
    <div class="caption" id="home-caption" data-aos="zoom-in" data-aos-duration="2000">
        <h1 id="demo"></h1>
        <p class="lead">
          <b>Sido Warass</b>
          website yang membahas secara rinci tentang Jamu Tradisional, mulai dari pengertian, sejarah, manfaat, contoh, hingga cara membuatnya.
        </p>
        <hr class="my-4">
        <p><i>Masih banyak kekurangan, menerima kritik & saran. Matur nuwun</i></p>
        <p class="closing">
            <a class="btn btn-primary btn-lg mt-10" href="sejarah.php" role="button">Selengkapnya</a>
        </p>
    </div>
</div>
<!-- ===== End Home ===== -->