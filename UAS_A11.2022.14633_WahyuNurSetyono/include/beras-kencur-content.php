<!-- =====  Beras Kencur ===== -->
<div class="container">
    <div class="caption" id="beras-kencur-caption">
        <h1 data-aos="fade-down" data-aos-duration="2000">Jamu Beras Kencur</h1>
        <br>
        <img src="assets/beras-kencur.jpg" alt="beras-kencur" data-aos="zoom-in" data-aos-duration="2000">
        <br>
        <ol type="A">
            <h4 data-aos="fade-down-right" data-aos-duration="2000"><li>Pengertian Jamu Beras Kencur</li></h4>
            <p data-aos="fade-down-left" data-aos-duration="2000">
                Beras kencur adalah minuman penyegar khas Jawa. Minuman ini juga digolongkan sebagai jamu karena memiliki khasiat meningkatkan nafsu makan.[1] Beras kencur sangat populer karena memiliki rasa yang manis dan segar. Bahan utama beras kencur tentu saja adalah beras (yang dihaluskan) dan kencur. Jamu beras kencur secara tradisional dijual bentuk cairan segar, baik di pasar-pasar umum atau oleh penjual jamu keliling. Industri jamu sekarang mengembangkan beras kencur yang dikemas dalam bentuk bubuk atau konsentrat (sirup) dan juga dijual bentuk bubuk kering instan tinggal menyeduh dengan air hangat atau air panas.
            </p>
                <br><br><br>
            <h4 data-aos="fade-down-right" data-aos-duration="2000"><li>Manfaat Jamu Beras Kencur</li></h4>
            <p data-aos="fade-down-left" data-aos-duration="2000">
                Kebanyakan masyarakat akrab dengan salah satu menu jamu tradisional, beras kencur. Tidak hanya kencur asli, tanaman kencur yang diolah bersama beras memiliki manfaat jika sudah berupa jamu tradisional. Tidak ada salahnya ketahui beberapa manfaat yang bisa kamu rasakan ketika mengonsumsi jamu beras kencur.
            </p>
                <h4 data-aos="fade-down-right" data-aos-duration="2000"><li>Cara Membuat Jamu Beras Kencur</li></h4>
            <ul style="list-style-type:circle;" data-aos="fade-down-left">
                Cara membuat jamu beras kencur membutuhkan ketelatenan tetapi tidak ribet. Berikut resep jamu beras kencur untuk 4 porsi :
                <br><br>                    
                <li>Bahan :</li>
                    <ul style="list-style-type: square;">
                        <li>1500 ml air</li>
                        <li>150 gram gula jawa</li>
                        <li>125 gram kencur</li>
                        <li>50 gram beras putih</li>
                        <li>5 sdm gula pasir</li>
                        <li>5 ruas jahe</li>
                    </ul>
                <br><br>
                <li>Cara Membuat :</li>
                    <ol type="1">
                        <li>Cuci bersih beras, kemudian rendam dalam air selama 1 jam. Setelah itu, sisihkan.</li>
                        <li>Rebus asam jawa, gula pasir, gula merah, dan jahe dengan air sampai mendidih. Setelah itu tunggu sampai agak dingin. Saring airnya. Sisihkan.</li>
                        <li>Cuci bersih kencur yang masih segar. Kupas kulitnya dan potong-potong.</li>
                        <li>Tiriskan air rendaman beras. Blender beras, kencur, dan air rebusan gula hingga halus.</li>
                        <li>Saring jamu beras kencur. Peras ampas blenderan sampai benar-benar kering. Sajikan jamu beras kencur.</li>
                    </ol>
            </ul>
        </ol>
    </div>
</div>
<!-- ===== End Beras Kencur ===== -->